package com.isika.cdi5.impairs;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;

public class NombreImpairsTest {

	@Test
	public void premierNombreImpairAvecEntreeZero() {
		NombreImpairs impairs = new NombreImpairs();
		List<Integer> impairsList = impairs.genererNombresImpairsInferieurs(0);
		assertEquals("Liste vide attendue", 0, impairsList.size());
	}
	
	@Test
	public void premierNombreImpair() {
		NombreImpairs impairs = new NombreImpairs();
		
		// le premier nb impair inférieur à 2 => 1
		// Je dois avoir en retour une liste avec 1 élément égal à 1
		List<Integer> impairsList = impairs.genererNombresImpairsInferieurs(2);
		assertEquals("Taille liste attendue 1", 1, impairsList.size());
		assertEquals(Integer.valueOf(1), impairsList.get(0));
	}
	
	@Test
	public void premierNombreImpairAvecDeuxElements() {
		NombreImpairs impairs = new NombreImpairs();
		
		// les premiers nbs impairs inférieurs à 4 => 1, 3
		// Je dois avoir en retour une liste avec 2 éléments
		List<Integer> impairsList = impairs.genererNombresImpairsInferieurs(4);
		assertEquals("Taille liste attendue 2", 2, impairsList.size());
		assertEquals(Integer.valueOf(1), impairsList.get(0));
		assertEquals(Integer.valueOf(3), impairsList.get(1));
	}

	@Test
	public void premierNombreImpairAvecTroisElements() {
		NombreImpairs impairs = new NombreImpairs();
		
		// les premiers nbs impairs inférieurs à 6 => 1, 3, 5
		// Je dois avoir en retour une liste avec 3 éléments
		List<Integer> impairsList = impairs.genererNombresImpairsInferieurs(6);
		assertEquals("Taille liste attendue 3", 3, impairsList.size());
		assertEquals(Integer.valueOf(1), impairsList.get(0));
		assertEquals(Integer.valueOf(3), impairsList.get(1));
		assertEquals(Integer.valueOf(5), impairsList.get(2));
	}
	
	@Test
	public void premierNombreImpairAvecPlusieursElements() {
		NombreImpairs impairs = new NombreImpairs();
		
		// les premiers nbs impairs inférieurs à 8 => 1, 3, 5, 7
		// Je dois avoir en retour une liste avec 4 éléments
		List<Integer> impairsList = impairs.genererNombresImpairsInferieurs(8);
		assertEquals("Taille liste attendue 4", 4, impairsList.size());
		assertEquals(Integer.valueOf(1), impairsList.get(0));
		assertEquals(Integer.valueOf(3), impairsList.get(1));
		assertEquals(Integer.valueOf(5), impairsList.get(2));
		assertEquals(Integer.valueOf(7), impairsList.get(3));
	}
}
