package com.isika.cdi5.fizzbuzz.app;

import java.util.ArrayList;
import java.util.List;

public class Main {

	public static void main(String[] args) {
		
		List<Integer> liste = new ArrayList<Integer>();
		for(int etape=1;etape<=15;etape++) {
			liste.add(etape);
		}
		
		// Comme j'ai testé tous les cas de la classe FizzBuzz, je peux l'utiliser directement en étant 
		// sûr de ne pas avoir de bugs, donc pas de lancement du main plusieurs fois, pas de débug pas à pas etc ...
		FizzBuzz fizzBuzz = new FizzBuzz();
		System.out.println(fizzBuzz.generate(liste));
	}
	
}
