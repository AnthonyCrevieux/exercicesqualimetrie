package com.isika.cdi5.fizzbuzz.app;

import java.util.List;

public class FizzBuzz {

	// Je regroupe les constantes
	private static final int CINQ = 5;
	private static final int TROIS = 3;
	private static final String BUZZ = "Buzz";
	private static final String FIZZ = "Fizz";

	public String remplacer(int nombreARemplacer) {
		// J'utilise les tags de désactivation de formatage auto pour cette section 
		// @formatter:off
		
		// Pour éviter de faire plusieurs "return" dans une méthode (c'est un large débat)
		// je peux introduire une variable locale et j'affecte le résultat à cette variable
		
		// Version avec Ifs
		if(estMultipleDe(nombreARemplacer, CINQ) 
				&& estMultipleDe(nombreARemplacer, TROIS)) 
			return FIZZ + BUZZ;
		
		if(estMultipleDe(nombreARemplacer, TROIS)) 
			return FIZZ;
		
		if(estMultipleDe(nombreARemplacer, CINQ)) 
			return BUZZ;
		
		return String.valueOf(nombreARemplacer);
		
		// @formatter:on
	}

	public String generate(final List<Integer> suiteFizzBuzz) {
		final StringBuilder builder = new StringBuilder();
		
		// J'utilise les streams Java 8 pour itérer sur ma liste, 
		// c'est une façon plus optimisée que la boucle for
		suiteFizzBuzz.stream().forEach(etape -> builder.append(remplacer(etape)));
		
		return builder.toString();
	}
	
	// Un peu d'encapsulation, je masque les détails techiques de calcul dans une méthode privée "réutilisable"
	private boolean estMultipleDe(int nombre, int multiple) {
		return nombre % multiple == 0;
	}

}
