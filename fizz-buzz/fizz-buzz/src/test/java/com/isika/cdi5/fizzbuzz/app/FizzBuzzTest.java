package com.isika.cdi5.fizzbuzz.app;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

// Ceci est une classe de test basée sur JUnit (framework de tests unitaires Java)
public class FizzBuzzTest {

	// Comme je n'ai pas besoin d'instancier la classe FizzBuzz dans tous les tests 
	// Alors je la déclare une fois et je l'utilise dans tous mes tests
	private FizzBuzz fizzBuzz = new FizzBuzz();

	// Ceci est une méthode de test : test unitaire
	// L'annotation @Test permet à la JVM de savoir comment lancer cette méthode
	// Le nom du test doit être parlant et explique le cas de test fonctionnel comme 
	// ça je n'ai pas besoin d'ajouter des commentaires
	@Test
	public void testSimpleCaseArgumentIsOne() {
		String remplacement = fizzBuzz .remplacer(1);
		
		// Les assertions permettent de vérifier le résultat d'un traitement 
		// Par rapport à une valeur attendue
		// Vous pouvez voir dans les imports les org.junit.Assert.* -> ça permet d'importer statiquement 
		// toutes les méthodes de type assertXXXX(.....)
		assertEquals("1", remplacement);
	}
	
	@Test
	public void testSimpleCaseArgumentIsTwo() {
		String remplacement = fizzBuzz.remplacer(2);
		assertEquals("2", remplacement);
	}
	
	@Test
	public void testSimpleCaseArgumentIsThree() {
		String remplacement = fizzBuzz.remplacer(3);
		assertEquals("Fizz", remplacement);
	}
	
	@Test
	public void testSimpleCaseArgumentIsFour() {
		String remplacement = fizzBuzz.remplacer(4);
		assertEquals("4", remplacement);
	}
	
	@Test
	public void testSimpleCaseArgumentIsFive() {
		String remplacement = fizzBuzz.remplacer(5);
		assertEquals("Buzz", remplacement);
	}
	
	@Test
	public void testSimpleCaseArgumentIsFifteen() {
		String remplacement = fizzBuzz.remplacer(15);
		assertEquals("FizzBuzz", remplacement);
	}
	
	@Test
	public void testGenerateFizzBuzzOneStep() {
		// Ici je peux laisser Arrays.asList(...) car je n'ai que 1 valeur, c'est assez lisible
		String remplacement = fizzBuzz.generate(Arrays.asList(1));
		assertEquals("1", remplacement);
	}
	
	@Test
	public void testGenerateFizzBuzzTwoSteps() {
		// Ici je peux laisser Arrays.asList(...) car je n'ai que 2 valeurs, c'est assez lisible
		String remplacement = fizzBuzz.generate(Arrays.asList(1, 2));
		assertEquals("12", remplacement);
	}
	
	@Test
	public void testGenerateFizzBuzzThreeSteps() {
		// Ici je peux laisser Arrays.asList(...) car je n'ai que 3 valeurs, c'est assez lisible
		String remplacement = fizzBuzz.generate(Arrays.asList(1, 2, 3));
		assertEquals("12Fizz", remplacement);
	}
	
	@Test
	public void testGenerateFizzBuzzFiveSteps() {
		String remplacement = fizzBuzz.generate(generateSuite(1, 5));
		assertEquals("12Fizz4Buzz", remplacement);
	}
	
	@Test
	public void testGenerateFizzBuzzFifteenSteps() {
		String remplacement = fizzBuzz.generate(generateSuite(1, 15));
		assertEquals("12Fizz4BuzzFizz78FizzBuzz11Fizz1314FizzBuzz", remplacement);
	}

	// Ici je vais créer une méthode privée qui me génère une liste d'entiers
	// Au lieu d'utiliser Arrays.asList(...) avec plus de 5 valeurs, c'est long à saisir :-)
	private List<Integer> generateSuite(int min, int max) {
		List<Integer> suite = new ArrayList<Integer>();
		
		// S'il y'a une seule instruction dans le for, je n'ai pas besoin des { } mais pour moins 
		// d'ambiguité je préfère les ajouter
		for(int step = min;step<=max;step++) {
			suite.add(step);
		}
		return suite;
	}
}
