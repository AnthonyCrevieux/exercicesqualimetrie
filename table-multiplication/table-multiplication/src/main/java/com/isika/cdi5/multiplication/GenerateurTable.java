package com.isika.cdi5.multiplication;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class GenerateurTable {

	private static final Integer[] MULTIPLICATEURS = {1, 2, 3, 4, 5, 6, 7, 8, 9};
	
	public List<Integer> multiples(int nombre) {
		List<Integer> multiples = new LinkedList<Integer>();
		Arrays.asList(MULTIPLICATEURS)
			.stream()
			.forEach(multiplicateur -> multiples.add(multiplicateur*nombre));
		return multiples;
	}

}
