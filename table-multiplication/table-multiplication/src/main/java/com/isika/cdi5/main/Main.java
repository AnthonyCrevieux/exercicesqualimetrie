package com.isika.cdi5.main;

import java.util.List;

import com.isika.cdi5.multiplication.GenerateurTable;

public class Main {

	public static void main(String[] args) {
		// Exercice 2 : Table de multiplication d'un nombre donné
		// Vous pouvez remplacer le nombre par celui que l'utilisateur va saisir
		// en utilisant un scanner avec un message
		int nombrePourTableMulti = 7;
		GenerateurTable generateur = new GenerateurTable();
		List<Integer> multiples = generateur.multiples(nombrePourTableMulti);
		multiples.stream().forEach(valeur -> System.out.println(valeur + " est un multiple de " + nombrePourTableMulti));
	}

}
