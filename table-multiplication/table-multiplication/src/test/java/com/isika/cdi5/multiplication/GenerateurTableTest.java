package com.isika.cdi5.multiplication;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;

public class GenerateurTableTest {

	private GenerateurTable generateur = new GenerateurTable();

	@Test
	public void multiplesDeNombreTousLesMultiples() {
		List<Integer> multiples = generateur.multiples(2);
		assertEquals(9, multiples.size());
		assertEquals(Integer.valueOf(2), multiples.get(0));
		assertEquals(Integer.valueOf(4), multiples.get(1));
		assertEquals(Integer.valueOf(6), multiples.get(2));
		assertEquals(Integer.valueOf(8), multiples.get(3));
		assertEquals(Integer.valueOf(10), multiples.get(4));
		assertEquals(Integer.valueOf(12), multiples.get(5));
		assertEquals(Integer.valueOf(14), multiples.get(6));
		assertEquals(Integer.valueOf(16), multiples.get(7));
		assertEquals(Integer.valueOf(18), multiples.get(8));
	}
	
	

}
